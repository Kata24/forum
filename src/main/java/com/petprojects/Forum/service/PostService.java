package com.petprojects.Forum.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.petprojects.Forum.model.Post;
import com.petprojects.Forum.model.User;
import com.petprojects.Forum.repository.PostRepository;
import com.petprojects.Forum.repository.UserRepository;

@Service
public class PostService {

	private PostRepository postRepository;
	private UserRepository userRepository;

	public PostRepository getPostRepository() {
		return postRepository;
	}

	@Autowired
	public void setPostRepository(PostRepository postRepository) {
		this.postRepository = postRepository;
	}

	public UserRepository getUserRepository() {
		return userRepository;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public List<Post> getMainpage() {
		return postRepository.findAll();
	}

	public Post getPost() {
		return postRepository.findFirstByOrderByPostedDesc();
	}

	public Post getSpecificPost(String title) {
		return postRepository.findByTitle(title);
	}

	public ArrayList<Post> findAll() {
		return (ArrayList<Post>) postRepository.findAll();
	}

	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	public String createPost(String title, String content, Date date, User userId) {
		try {
			Post post = new Post(title, content, new Date(), userId);
			postRepository.save(post);
			return "success";
		} catch (Exception exception) {
			return "Unexpected error. Couldn't save post to DB.";
		}
	}

	// public Object createPost() {
	// TODO Auto-generated method stub
	// return null;
	// }

}
