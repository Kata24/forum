package com.petprojects.Forum.service;

import org.springframework.stereotype.Service;

import com.petprojects.Forum.model.User;

@Service
	public interface UserService {

		void addUser(User user);
	}

