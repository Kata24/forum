package com.petprojects.Forum.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import com.petprojects.Forum.model.Post;
import com.petprojects.Forum.model.User;



public interface PostRepository extends CrudRepository<Post, Long> {

	//SELECT * FROM POST 
	List<Post> findAll();
	
	void delete(Post deleted);
 
   // Optional<Post> findOne(Long id);
    
    //<S extends T> S save(S entity);
    <p extends Post>p save(p persisted);
	
	Post findFirstByOrderByPostedDesc();

	Post findByTitle(String title);
	
	Post findByUser(User user);
	
	
	
}