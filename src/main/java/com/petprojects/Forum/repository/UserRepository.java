package com.petprojects.Forum.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.petprojects.Forum.model.User;

public interface UserRepository extends CrudRepository<User, Long> {

	List<User> findAll();

	void saveAndFlush(User user);
}
