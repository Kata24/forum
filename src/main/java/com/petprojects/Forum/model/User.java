package com.petprojects.Forum.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class User {

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private int age;
	@OneToMany(mappedBy = "user")
	private List<Post> mainpage;

	public User() {
	}

	public User(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public List<Post> getMainpage() {
		return mainpage;
	}

	public void setMainpage(List<Post> mainpage) {
		this.mainpage = mainpage;
	}

}