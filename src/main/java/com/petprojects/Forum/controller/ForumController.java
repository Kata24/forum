package com.petprojects.Forum.controller;

import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.petprojects.Forum.model.Post;
import com.petprojects.Forum.service.PostService;

@Controller
public class ForumController {

	private PostService postService;

	@Autowired
	public void setPostService(PostService postService) {
		this.postService = postService;
	}

	@RequestMapping("/")
	public String mainpage(Model model, Locale locale) {
		model.addAttribute("title", "My forum");
		model.addAttribute("mainpage", postService.getMainpage());
		return "mainpage";
	}

	@RequestMapping("/title/{title}")
	public String searchForUser(@PathVariable(value = "title") String title, Model model, Locale locale)
			throws Exception {
		model.addAttribute("mainpage", postService.getSpecificPost(title));
		if (title == null)
			throw new Exception("Nincs ilyen címmel bejegyzés!");

		return "mainpage";
	}

	@GetMapping("/createpost")
	public String createPost(Model model) {
		model.addAttribute("createPost", postService.getAllUsers());
		return "createpost";
	}

	@PostMapping("/createpost")
	@ResponseBody
	public String submitPost(Model model, Locale locale, Post post) {
		model.addAttribute("createPost", postService.createPost(post.getTitle(), post.getContent(), new Date(), post.getUser()));

		return "mainpage"; 
	}

	@ExceptionHandler(Exception.class)
	public String exceptionHandler(HttpServletRequest rA, Exception ex, Model model) {
		model.addAttribute("errMessage", ex.getMessage());
		return "exceptionHandler";
	}

}
