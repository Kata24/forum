package com.petprojects.Forum.controller;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("org.example.base")
@EntityScan("org.example.base.entities")
public class MyConfig {

}