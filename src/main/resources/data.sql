

INSERT INTO USER ( id, age, name ) VALUES ( 1, 28, 'BENJI');
INSERT INTO USER ( id, age, name ) VALUES ( 2, 48, 'Mr.Smidt');
INSERT INTO USER ( id, age, name ) VALUES ( 3, 29, 'Mary');
 
INSERT INTO POST ( id, content, posted, title, user_id  ) VALUES ( 4, 'story1', CURRENT_DATE(),'Title1', (select id FROM USER where name = 'BENJI'));
INSERT INTO POST ( id, content, posted, title, user_id  ) VALUES ( 5, 'ansver1',CURRENT_DATE(),'Title2', (select id FROM USER where name = 'Mr.Smidt'));
INSERT INTO POST ( id, content, posted, title, user_id  ) VALUES ( 6, 'story3', CURRENT_DATE(),'Title3', (select id FROM USER where name = 'BENJI'));
INSERT INTO POST ( id, content, posted, title, user_id  ) VALUES ( 7, 'story1', CURRENT_DATE(),'Title4', (select id FROM USER where name = 'Mary'));
INSERT INTO POST ( id, content, posted, title, user_id  ) VALUES ( 8, 'ansver2',CURRENT_DATE(),'Title5', (select id FROM USER where name = 'Mr.Smidt'));
INSERT INTO POST ( id, content, posted, title, user_id  ) VALUES ( 9, 'story4', CURRENT_DATE(),'Title6', (select id FROM USER where name = 'Mary')); 